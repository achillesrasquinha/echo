-include .env

ENVIRONMENT			   ?= development

BASEDIR				   := $(realpath .)
PROJECT				   ?= echo
PROJDIR				   := $(BASEDIR)/$(PROJECT)

PIPENV				   ?= $(shell which pipenv)
ENVBIN				   ?= $(shell $(PIPENV) --venv)/bin

PYTHON				   ?= $(ENVBIN)/python
PYTEST				   ?= $(ENVBIN)/pytest
PYLINT				   ?= $(ENVBIN)/pylint
PGCLI				   ?= $(ENVBIN)/pgcli

YARN				   ?= $(shell which yarn)

NODEMOD				   := $(BASEDIR)/node_modules
NODEBIN				   := $(NODEMOD)/.bin

WEBPACK				   := $(NODEBIN)/webpack

clean:
	find $(BASEDIR) | grep -E "__pycache__|\.pyc" | xargs rm -rf

	rm -rf \
		$(BASEDIR)/.pytest_cache \
		$(BASEDIR)/*-error.log 

	clear

install:
	make clean

	$(PIPENV) install
	$(YARN)   install

migrate:
	make clean

	$(PYTHON) $(PROJDIR)/manage.py makemigrations
	$(PYTHON) $(PROJDIR)/manage.py migrate

build:
	ln -sf $(BASEDIR)/.env $(PROJDIR)/.env

ifeq ($(watch),)
	$(eval watch := false)
else
	$(eval watch := true)
endif

	make migrate &
		$(WEBPACK) \
			--mode 	 $(ENVIRONMENT)						\
			--config $(BASEDIR)/webpack.config.babel.js \
			--watch  $(watch)

test:
	make clean

	$(PYTEST) $(PROJDIR) --cov=$(PROJDIR) 

ifneq ($(lint),)
	$(PYLINT) --load-plugins pylint_django $(PROJDIR) --disable=C0326
endif

shell:
	make clean

	$(PYTHON) $(PROJDIR)/manage.py shell -i ipython

dbshell:
	make clean

	$(PGCLI) \
		--host	   $(ECHO_DB_HOST) \
		--port 	   $(ECHO_DB_PORT) \
		--username $(ECHO_DB_USER) \
		--no-password			   \
		--dbname   $(ECHO_DB_NAME) \
		--less-chatty 			   \
		--prompt   "\d> "

start:
	make clean

	$(PYTHON) $(PROJDIR)/manage.py runserver &
		make build watch=true