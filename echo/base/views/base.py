from django.shortcuts import render
from base.models import Quote

def index(request):
    quotes   = Quote.objects.all()

    context  = dict(quotes = quotes)
    template = render(request, 'base/pages/index.html', context)
    
    return template

def about(request):
    template = render(request, 'base/pages/about.html')
    return template