import logging

import requests as req

from base.models import Quote, Author

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)

def create():
    res = req.get('http://bit.do/quotes-5000')
    if res.ok:
        quotes = res.json()
        for quote in quotes:
            res    = req.get('https://picsum.photos/1024/728/?random')
            if res.ok:
                author = Author(
                    name = quote['quoteAuthor']
                )
                author.save()
                
                model  = Quote(
                    content = quote['quoteText'],
                    author  = author,
                    image   = res.url
                )
                log.debug("Saving {quote}".format(quote = model))
                model.save()
    else:
        res.raise_for_status()