from echo import util

def test_ellipsis():
    assert util.ellipsis('foobar', length = 3) == 'foo...'
    assert util.ellipsis('foobar', length = 4) == 'foob...'

    assert util.ellipsis('foobar', length = 3, count = 4) == 'foo....'

    assert util.ellipsis('foobar', length = 3, style = ',') == 'foo,,,'