import os
import os.path as osp

import dotenv

from echo.system import pardir

DIRECTORY           = { }
DIRECTORY['base']   = pardir(__file__, 2)
DIRECTORY['assets'] = osp.join(DIRECTORY['base'], 'assets')

dotenv.load_dotenv(osp.join(DIRECTORY['base'], '.env'))

DEBUG               = os.environ.get('ECHO_ENVIRONMENT', 'development') == 'development'

INSTALLED_APPS      = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'rest_framework',
    'django_pgcli',

    'echo',
    'base'
]

TEMPLATES  = [
    {
        'BACKEND': 'django.template.backends.jinja2.Jinja2',
        'DIRS': [
            osp.join(DIRECTORY['base'], 'base', 'templates')
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'environment': 'base.jinja2.environment'
        }
    },
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [ ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

DATABASES  = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.environ.get('ECHO_DB_NAME', 'echo'),
        'USER': os.environ.get('ECHO_DB_USER', 'admin'),
        'PASS': os.environ.get('ECHO_DB_PASS', 'slartibartfast'),
        'HOST': os.environ.get('ECHO_DB_HOST', 'localhost'),
        'PORT': os.environ.get('ECHO_DB_PORT', '')
    }
}

STATIC_URL       = '/assets/'
STATICFILES_DIRS = [
    DIRECTORY['assets']
]

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'ii&&08(83*m06ju$w5v)tdy((h7=6n9@k)evgw=530311g-q41'

ALLOWED_HOSTS = []

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'echo.urls'



WSGI_APPLICATION = 'echo.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases




# Password validation
# https://docs.djangoproject.com/en/1.11/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/1.11/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True